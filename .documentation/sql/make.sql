﻿	EXPLAIN
SELECT
  m0_.id AS id_0,
  m0_.code AS code_1,
  m0_.description AS description_2,
  m0_.vehicle_type_id AS vehicle_type_id_3
FROM make m0_
  INNER JOIN vehicle_type v1_
    ON m0_.vehicle_type_id = v1_.id
WHERE v1_.code = ?
ORDER BY m0_.description ASC