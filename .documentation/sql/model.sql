﻿explain
SELECT
  m0_.id AS id_0,
  m0_.description AS description_1,
  m0_.make_id AS make_id_2,
  m0_.vehicle_type_id AS vehicle_type_id_3
FROM model m0_
  INNER JOIN vehicle_type v1_
    ON m0_.vehicle_type_id = v1_.id
  INNER JOIN make m2_
    ON m0_.make_id = m2_.id
WHERE v1_.code = ?
AND m2_.code = ?
ORDER BY m0_.description ASC