﻿--
-- Скрипт сгенерирован Devart dbForge Studio 2019 for MySQL, Версия 8.2.23.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 19.04.2020 15:16:04
-- Версия сервера: 5.7.25
-- Версия клиента: 4.1
--

-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE `vehicles-db-test`;

--
-- Удалить таблицу `migration_versions`
--
DROP TABLE IF EXISTS migration_versions;

--
-- Удалить таблицу `search_log`
--
DROP TABLE IF EXISTS search_log;

--
-- Удалить таблицу `model`
--
DROP TABLE IF EXISTS model;

--
-- Удалить таблицу `make`
--
DROP TABLE IF EXISTS make;

--
-- Удалить таблицу `vehicle_type`
--
DROP TABLE IF EXISTS vehicle_type;

--
-- Установка базы данных по умолчанию
--
USE `vehicles-db-test`;

--
-- Создать таблицу `vehicle_type`
--
CREATE TABLE vehicle_type (
  id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  code char(1) NOT NULL,
  description varchar(255) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 27,
AVG_ROW_LENGTH = 1260,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_unicode_ci;

--
-- Создать индекс `UK_vehicle_type_code` для объекта типа таблица `vehicle_type`
--
ALTER TABLE vehicle_type
ADD UNIQUE INDEX UK_vehicle_type_code (code);

--
-- Создать таблицу `make`
--
CREATE TABLE make (
  id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  vehicle_type_id int(10) UNSIGNED DEFAULT NULL,
  code char(4) NOT NULL,
  description varchar(255) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 3095,
AVG_ROW_LENGTH = 74,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_unicode_ci;

--
-- Создать индекс `FK_make_vehicle_type_id` для объекта типа таблица `make`
--
ALTER TABLE make
ADD INDEX FK_make_vehicle_type_id (vehicle_type_id);

--
-- Создать внешний ключ
--
ALTER TABLE make
ADD CONSTRAINT make_vehicle_type_id_vehicle_type_id FOREIGN KEY (vehicle_type_id)
REFERENCES vehicle_type (id) ON DELETE CASCADE;

--
-- Создать таблицу `model`
--
CREATE TABLE model (
  id int(11) NOT NULL AUTO_INCREMENT,
  make_id int(10) UNSIGNED DEFAULT NULL,
  vehicle_type_id int(10) UNSIGNED DEFAULT NULL,
  description varchar(255) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 2615,
AVG_ROW_LENGTH = 62,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_unicode_ci;

--
-- Создать индекс `FK_model_make_id` для объекта типа таблица `model`
--
ALTER TABLE model
ADD INDEX FK_model_make_id (make_id);

--
-- Создать индекс `FK_model_vehicle_type_id` для объекта типа таблица `model`
--
ALTER TABLE model
ADD INDEX FK_model_vehicle_type_id (vehicle_type_id);

--
-- Создать внешний ключ
--
ALTER TABLE model
ADD CONSTRAINT model_make_id_make_id FOREIGN KEY (make_id)
REFERENCES make (id) ON DELETE CASCADE;

--
-- Создать внешний ключ
--
ALTER TABLE model
ADD CONSTRAINT model_vehicle_type_id_vehicle_type_id FOREIGN KEY (vehicle_type_id)
REFERENCES vehicle_type (id) ON DELETE CASCADE;

--
-- Создать таблицу `search_log`
--
CREATE TABLE search_log (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  vehicle_type_code char(1) NOT NULL,
  make_code char(4) NOT NULL,
  found_results_count int(10) UNSIGNED NOT NULL,
  request_time datetime NOT NULL,
  user_ip varbinary(16) NOT NULL COMMENT 'Supports IPv6',
  user_agent varchar(255) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_unicode_ci;

--
-- Создать индекс `search_log_request_time_index` для объекта типа таблица `search_log`
--
ALTER TABLE search_log
ADD INDEX search_log_request_time_index (request_time);

--
-- Создать таблицу `migration_versions`
--
CREATE TABLE migration_versions (
  version varchar(14) NOT NULL,
  executed_at datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (version)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_unicode_ci;

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;