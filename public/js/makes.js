$(function () {
    var resultsBlock = $('#results')
    var noResultsBlock = $('#no-results')
    var template = $('#results [value="template"]')
    template.hide()

    $('#makes-list').change(function (ev) {
        setDefaultState();

        var value = $(this).val()
        if ('' === value) {
            return
        }
        $.ajax({
            url: '/makes/' + vehicleTypeCode + '/' + value,
            method: 'get',
            dataType: 'json'
        }).done(function (result) {
            if (result.length) {
                setResultsState(result)
            } else {
                setNotFoundState()
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert('ACHTUNG!: ' + textStatus)
        });
    })

    function setDefaultState() {
        resultsBlock.hide()
        noResultsBlock.hide()
    }

    function setNotFoundState() {
        resultsBlock.hide()
        noResultsBlock.show()
    }

    function setResultsState(results) {

        $(template).parent().find('[value!="template"]').remove();

        $(results.reverse()).each(function (index, element) {
            var el = $(template).clone(false);
            el.removeAttr('value')
            el.text(element.description)
            $(template).after(el)
            el.show()
        })

        resultsBlock.show()
    }
})