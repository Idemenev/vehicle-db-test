<?php

namespace App\Controller;

use App\Services\MakeService;
use App\Services\ModelService;
use App\Services\SearchLogService;
use App\Services\VehicleTypeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class VehicleController extends AbstractController
{
    /**
     * A list of vehicle types
     * @param VehicleTypeService $vehicleTypeService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function vehicleTypes(VehicleTypeService $vehicleTypeService)
    {
        $result = $vehicleTypeService->getVehicleTypesList();

        return $this->render('vehicle/vehicle_types.html.twig', [
            'controller_name' => 'VehicleController',
            'vehicle_types' => $result,
        ]);
    }

    /**
     * @param string $typeCode
     * @param MakeService $makeService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function makes(string $typeCode, MakeService $makeService)
    {
        $result = $makeService->getListByVehicleTypeCode($typeCode);

        return $this->render('vehicle/makes.html.twig', [
            'controller_name' => 'VehicleController',
            'makes' => $result,
            'vehicle_type_code' => $typeCode,
        ]);
    }

    /**
     * @param string $typeCode
     * @param string $makeCode
     * @param ModelService $modelService
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function models(string $typeCode, string $makeCode, ModelService $modelService, SearchLogService $searchLogService)
    {
        $result = $modelService->getListByVehicleTypeCodeAndMakeCode($typeCode, $makeCode);

        // You should log all request for ___models route___ to database (SearchLog entity)
        $searchLogService->log($typeCode, $makeCode, count($result));

        // @todo вынести в хелпер
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $data = $serializer->normalize($result, null, ['attributes' => ['description']]);

        return $this->json($data);
    }
}
