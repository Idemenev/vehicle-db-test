<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200419142915 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Initial migration. Patient #0';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE search_log (id BIGINT AUTO_INCREMENT NOT NULL, vehicle_type_code CHAR(1) NOT NULL, make_code CHAR(4) NOT NULL, found_results_count INT UNSIGNED NOT NULL, request_time DATETIME NOT NULL, user_ip VARBINARY(16) NOT NULL COMMENT \'Supports IPv6\', user_agent VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');

        $this->addSql('ALTER TABLE search_log ADD INDEX search_log_request_time_index (request_time DESC)');

        $this->addSql('CREATE TABLE vehicle_type (id INT UNSIGNED AUTO_INCREMENT NOT NULL, code CHAR(1) NOT NULL, description VARCHAR(255) NOT NULL, UNIQUE INDEX UK_vehicle_type_code (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');

        $this->addSql('CREATE TABLE make (id INT UNSIGNED AUTO_INCREMENT NOT NULL, vehicle_type_id INT UNSIGNED DEFAULT NULL, code CHAR(4) NOT NULL, description VARCHAR(255) NOT NULL, INDEX FK_make_vehicle_type_id (vehicle_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');

        $this->addSql('CREATE TABLE model (id INT AUTO_INCREMENT NOT NULL, make_id INT UNSIGNED DEFAULT NULL, vehicle_type_id INT UNSIGNED DEFAULT NULL, description VARCHAR(255) NOT NULL, INDEX FK_model_make_id (make_id), INDEX FK_model_vehicle_type_id (vehicle_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');

        $this->addSql('ALTER TABLE make ADD CONSTRAINT make_vehicle_type_id_vehicle_type_id FOREIGN KEY (vehicle_type_id) REFERENCES vehicle_type (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE model ADD CONSTRAINT model_make_id_make_id FOREIGN KEY (make_id) REFERENCES make (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE model ADD CONSTRAINT model_vehicle_type_id_vehicle_type_id FOREIGN KEY (vehicle_type_id) REFERENCES vehicle_type (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // drops order make sense!
        $this->addSql('DROP TABLE model');
        $this->addSql('DROP TABLE make');
        $this->addSql('DROP TABLE vehicle_type');
        $this->addSql('DROP TABLE search_log');
    }
}
