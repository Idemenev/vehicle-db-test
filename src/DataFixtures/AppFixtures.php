<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectManager;
use App\Entity\VehicleType;
use App\Entity\Make;
use App\Entity\Model;

class AppFixtures extends Fixture
{
    private const DATA_FILE_VEHICLE_TYPE = 'vehicle_types';
    private const DATA_FILE_MAKE = 'makes';
    private const DATA_FILE_MODEL = 'models';

    /**
     * @var string
     */
    private $fixturesDataPath;

    /** @var EntityManager */
    private $entityManager;

    /**
     * @var array
     */
    private $tableReferences = [];

    /**
     * AbstractAppFixture constructor.
     * @param string $fixturesDataPath
     */
    public function __construct(string $fixturesDataPath)
    {
        $this->fixturesDataPath = $fixturesDataPath;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->entityManager = $manager;

        // order matters!
        $this->loadVehicleType();
        $this->loadMake();
        $this->loadModel();
    }

    /**
     * @param string $fileBaseName
     * @return array
     * @throws \Exception
     */
    private function getData(string $fileBaseName): array
    {
        $absPath = $this->getFilePath($fileBaseName);

        if (!file_exists($absPath) || !is_readable($absPath)) {
            throw new \Exception(sprintf('Fixture file "%s" doesn\'t exists', $absPath));
        }
        return json_decode(file_get_contents($absPath), true,512, JSON_THROW_ON_ERROR);
    }

    /**
     * @param string $fileBaseName
     * @return string
     */
    private function getFilePath(string $fileBaseName): string
    {
        return $this->fixturesDataPath . '/' . $fileBaseName . '.json';
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function loadVehicleType(): void
    {
        $data = $this->getData(self::DATA_FILE_VEHICLE_TYPE);

        foreach ($data as $v) {
            $entity = new VehicleType();
            $entity
                ->setCode($v['code'])
                ->setDescription($v['description']);

            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();

        $this->tableReferences['VehicleType'] = $this->entityManager->getRepository(VehicleType::class)
            ->findAllIndexBy('code');
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function loadMake(): void
    {
        $data = $this->getData(self::DATA_FILE_MAKE);

        foreach ($data as $v) {
            if (isset($this->tableReferences['VehicleType'][$v['type']])) { // как минимум нет в файле vehicle_codes, записи с code == "I" ! WTF?????
                $vehicleType = $this->tableReferences['VehicleType'][$v['type']];
            } else {
                $vehicleType = null;
            }
            $entity = new Make();
            $entity->setCode($v['code']);
            $entity->setDescription($v['description']);
            $entity->setVehicleType($vehicleType);

            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();

        $this->tableReferences['Make'] = $this->entityManager->getRepository(Make::class)
            ->findAllIndexBy('code');
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function loadModel(): void
    {
        $data = $this->getData(self::DATA_FILE_MODEL);

        foreach ($data as $v) {
            if (isset($this->tableReferences['VehicleType'][$v['type']])) {
                $vehicleType = $this->tableReferences['VehicleType'][$v['type']];
            } else {
                $vehicleType = null;
            }
            if (isset($this->tableReferences['Make'][$v['group']])) {
                $make = $this->tableReferences['Make'][$v['group']];
            } else {
                $make = null;
            }

            $entity = new Model();
            $entity->setDescription($v['description']);
            $entity->setVehicleType($vehicleType);
            $entity->setMake($make);

            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();

        $this->tableReferences['Make'] = $this->entityManager->getRepository(Make::class)
            ->findAllIndexBy('code');
    }
}
