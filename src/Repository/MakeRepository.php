<?php

namespace App\Repository;

use App\Entity\Make;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Make|null find($id, $lockMode = null, $lockVersion = null)
 * @method Make|null findOneBy(array $criteria, array $orderBy = null)
 * @method Make[]    findAll()
 * @method Make[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MakeRepository extends AbstractAppRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Make::class);
    }

    public function findByVehicleTypeCode(string $vehicleTypeCode)
    {
        $q = $this
            ->createQueryBuilder('m')
            ->select('m')
            ->innerJoin('m.vehicleType', 'vt')
            ->where('vt.code = :vehicleType')
            ->setParameter('vehicleType', $vehicleTypeCode)
            ->orderBy('m.' . Make::DEFAULT_SORT_BY)
            ->getQuery();
        $result = $q->getResult();

        return $result;
    }
}
