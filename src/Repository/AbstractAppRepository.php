<?php

namespace App\Repository;

use App\Entity\Make;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Make|null find($id, $lockMode = null, $lockVersion = null)
 * @method Make|null findOneBy(array $criteria, array $orderBy = null)
 * @method Make[]    findAll()
 * @method Make[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
abstract class AbstractAppRepository extends ServiceEntityRepository
{
    private const DUMMY_ALIAS = 'foo';

    private const SORT_ASC = 'ASC';
    private const SORT_DESC = 'DESC';

    /**
     * @param string $indexField поле, по которому строить индекс результата
     * @return mixed
     */
    public function findAllIndexBy(string $indexField)
    {
        return $this->createQueryBuilder(self::DUMMY_ALIAS, self::DUMMY_ALIAS . '.' . $indexField)
            ->getQuery()
            ->getResult();
    }

    public function findAllSortBy(string $column, string $direction = self::SORT_ASC)
    {
        return $this->findBy([], [$column => $direction]);
    }
}
