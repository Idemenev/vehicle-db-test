<?php

namespace App\Repository;

use App\Entity\Model;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Model|null find($id, $lockMode = null, $lockVersion = null)
 * @method Model|null findOneBy(array $criteria, array $orderBy = null)
 * @method Model[]    findAll()
 * @method Model[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModelRepository extends AbstractAppRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Model::class);
    }

    public function findByVehicleTypeCodeAndMakeCode(string $vehicleTypeCode, string $makeCode)
    {
        // на больших таблицах имеет смысл получать тремя отдельными запросами
        $q = $this
            ->createQueryBuilder('t')
            ->select('t')
            ->innerJoin('t.vehicleType', 'vt')
            ->innerJoin('t.make', 'm')
            ->where('vt.code = :vehicleType')
            ->andWhere('m.code = :makeCode')
            ->setParameter('vehicleType', $vehicleTypeCode)
            ->setParameter('makeCode', $makeCode)
            ->orderBy('t.' . Model::DEFAULT_SORT_BY)
            ->getQuery();
        $result = $q->getResult();

        return $result;
    }
}
