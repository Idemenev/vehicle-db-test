<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SearchLog
 *
 * @ORM\Table(name="search_log")
 * @ORM\Entity
 */
class SearchLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="vehicle_type_code", type="string", length=1, nullable=false, options={"fixed"=true})
     */
    private $vehicleTypeCode;

    /**
     * @var string
     *
     * @ORM\Column(name="make_code", type="string", length=4, nullable=false, options={"fixed"=true})
     */
    private $makeCode;

    /**
     * @var int
     *
     * @ORM\Column(name="found_results_count", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $foundResultsCount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="request_time", type="datetime", nullable=false)
     */
    private $requestTime;

    /**
     * @var binary
     *
     * @ORM\Column(name="user_ip", type="binary", nullable=false, options={"comment"="Supports IPv6"})
     */
    private $userIp;

    /**
     * @var string
     *
     * @ORM\Column(name="user_agent", type="string", length=255, nullable=false)
     */
    private $userAgent;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getVehicleTypeCode(): ?string
    {
        return $this->vehicleTypeCode;
    }

    public function setVehicleTypeCode(string $vehicleTypeCode): self
    {
        $this->vehicleTypeCode = $vehicleTypeCode;

        return $this;
    }

    public function getMakeCode(): ?string
    {
        return $this->makeCode;
    }

    public function setMakeCode(string $makeCode): self
    {
        $this->makeCode = $makeCode;

        return $this;
    }

    public function getFoundResultsCount(): ?int
    {
        return $this->foundResultsCount;
    }

    public function setFoundResultsCount(int $foundResultsCount): self
    {
        $this->foundResultsCount = $foundResultsCount;

        return $this;
    }

    public function getRequestTime(): ?\DateTimeInterface
    {
        return $this->requestTime;
    }

    public function setRequestTime(\DateTimeInterface $requestTime): self
    {
        $this->requestTime = $requestTime;

        return $this;
    }

    public function getUserIp()
    {
        return inet_ntop($this->userIp);
    }

    /**
     * @param string $userIp в строком значении 127.0.0.1
     * @return SearchLog
     */
    public function setUserIp(string $userIp): self
    {
        $this->userIp = inet_pton($userIp);

        return $this;
    }

    public function getUserAgent(): ?string
    {
        return $this->userAgent;
    }

    public function setUserAgent(string $userAgent): self
    {
        $this->userAgent = $userAgent;

        return $this;
    }


}
