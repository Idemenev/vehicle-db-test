<?php

namespace App\Entity;

abstract class AbstractEntity
{
    public const DEFAULT_SORT_BY = 'description';
}