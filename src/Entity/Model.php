<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Model
 *
 * @ORM\Table(name="model", indexes={@ORM\Index(name="FK_model_make_id", columns={"make_id"}), @ORM\Index(name="FK_model_vehicle_type_id", columns={"vehicle_type_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\ModelRepository")
 */
class Model extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var \Make
     *
     * @ORM\ManyToOne(targetEntity="Make")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="make_id", referencedColumnName="id")
     * })
     */
    private $make;

    /**
     * @var \VehicleType
     *
     * @ORM\ManyToOne(targetEntity="VehicleType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vehicle_type_id", referencedColumnName="id")
     * })
     */
    private $vehicleType;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getMake(): ?Make
    {
        return $this->make;
    }

    public function setMake(?Make $make): self
    {
        $this->make = $make;

        return $this;
    }

    public function getVehicleType(): ?VehicleType
    {
        return $this->vehicleType;
    }

    public function setVehicleType(?VehicleType $vehicleType): self
    {
        $this->vehicleType = $vehicleType;

        return $this;
    }


}
