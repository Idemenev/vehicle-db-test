<?php

namespace App\Services;

use App\Entity\Make;

class MakeService extends AbstractService
{
    /**
     * @param string $typeCode
     * @return \App\Entity\Make[]
     */
    public function getListByVehicleTypeCode(string $typeCode) : array
    {
        return $this->getEntityManager()->getRepository(Make::class)
            ->findByVehicleTypeCode($typeCode);
    }
}