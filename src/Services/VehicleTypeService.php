<?php

namespace App\Services;

use App\Entity\VehicleType;

class VehicleTypeService extends AbstractService
{
    /**
     * @return \App\Entity\VehicleType[]
     */
    public function getVehicleTypesList() : array
    {
        return $this->getEntityManager()->getRepository(VehicleType::class)
            ->findAllSortBy(VehicleType::DEFAULT_SORT_BY);
    }
}