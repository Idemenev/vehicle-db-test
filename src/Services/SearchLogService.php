<?php

namespace App\Services;

use App\Entity\SearchLog;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;

class SearchLogService extends AbstractService
{
    /**
     * @var Request
     */
    private $request;

    public function __construct(EntityManager $entityManager, RequestStack $requestStack)
    {
        parent::__construct($entityManager);
        $this->request = $requestStack->getCurrentRequest();
    }

    public function log(string $typeCode, string $makeCode, int $foundResultsCount)
    {
        $searchLog = new SearchLog();

        // @todo в целом, не должны падать и отказывать юзеру в обслуживании, в случае падения записи лога. наверное. чёт делаем. пишем в monolog, пушим в sentry
        try {
            $searchLog->setVehicleTypeCode($typeCode)
                ->setMakeCode($makeCode)
                ->setFoundResultsCount($foundResultsCount)
                ->setUserIp($this->request->getClientIp())
                ->setUserAgent($this->request->headers->get('User-Agent'))
                ->setRequestTime(new \DateTime());

                $this->getEntityManager()->persist($searchLog);
                $this->getEntityManager()->flush();
        } catch (DBALException $e) {

        } catch (\Throwable $e) {
            // трам пам пам
        }
    }
}