<?php

namespace App\Services;

use App\Entity\Model;

class ModelService extends AbstractService
{
    /**
     * @param string $typeCode
     * @return \App\Entity\Model[]
     */
    public function getListByVehicleTypeCodeAndMakeCode(string $typeCode, string $makeCode) : array
    {
        return $this->getEntityManager()->getRepository(Model::class)
            ->findByVehicleTypeCodeAndMakeCode($typeCode, $makeCode);
    }
}